# ack.vim for Pearl

Vim plugin for the Perl module / CLI script 'ack'

## Details

- Plugin: https://github.com/mileszs/ack.vim
- Pearl: https://github.com/pearl-core/pearl
